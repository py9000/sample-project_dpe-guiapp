import pytest
import click

from sampleprojectdpewebapp.settings import server_port, socket_host, CONFIG_DIC, BASEDIR

from sampleprojectdpewebapp.cherrypy_app import cherrypy as cherrypy_app
from sampleprojectdpewebapp.flask_app import app as flask_app


@click.group()
def cli():
    return None

@click.group()
def servers():
    return None

@click.group()
def testing():
    return None


@click.group()
def helpers():
    return None

@testing.command()
def selftest():
    pytest.main(["-x", "-v", BASEDIR])


@testing.command()
def selfcoverage():
    pytest.main(["--cov", BASEDIR, "--cov-report", "term-missing"])

@cli.command()
def version():
    print_version()


@servers.command()
def runcherrypyserver():
    cherrypy_app.engine.start()


@servers.command()
def runflaskserver():
    flask_app.run(host=socket_host, port=server_port)

cli.add_command(servers)
cli.add_command(testing)
cli.add_command(helpers)
main = cli
