import subprocess

from flask import Blueprint
from flask import render_template, jsonify, make_response
from flask_security.core import current_user
from htmlmin.minify import html_minify

gui_blueprint = Blueprint("gui_blueprint", __name__, template_folder="templates")  # noqa: E501


@gui_blueprint.route("/", methods=["GET"])
# @login_required
# @flask_profiler.profile()
def index():
    # return render_template('index.html', name=name, title="Dashboard")
    return html_minify(render_template("index.html", title="Dashboard"))


@gui_blueprint.route("/system/health", methods=["GET"])
# @login_required
def system_health():
    return jsonify({"status": "healthy"})


@gui_blueprint.route("/system/info", methods=["GET"])
# @login_required
def system_info():
    info = subprocess.Popen("lsb_release -a", shell=True, stdout=subprocess.PIPE).stdout.read()
    return info


@gui_blueprint.route("/system/test")
# @login_required
def system_test():
    return render_template("test.html", title="test")
