base:
  '*':
    - apt_requirements
    - app
    - postgresql
    - mongo
    - memcached
    - nginx
    - supervisord
    - finalize
