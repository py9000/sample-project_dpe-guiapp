install_supervisord:
  cmd.run:
    - name: sudo apt -y install supervisor

supervisord_config_file:
  file.managed:
    - name: /etc/supervisor/conf.d/sampleprojectdpewebapp.conf
    - source: salt://supervisord/sampleprojectdpewebapp.conf.template

start_supervisord_service_daemon:
  cmd.run:
    - name: "sudo service supervisor start"

start_supervisord_service_watcher:
  cmd.run:
    - name: "sudo supervisorctl reread; sudo supervisorctl update"
